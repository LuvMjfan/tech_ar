#!flask/bin/python


import os
from app import app

if __name__ == '__main__':
    if os.environ.get('APP_SETTINGS') == 'config.DevelopmentConfig':
        app.run(debug=True)

