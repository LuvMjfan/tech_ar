# app/__init__.py

import os

from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object(os.environ.get('APP_SETTINGS'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/ram/Desktop/lokesh.sqlite'
app.secret_key = 'kutta'
db = SQLAlchemy(app)

from app.controllers.admin import admin
from app.controllers.public import public

from app import models
from .models import User
from flask_migrate import Migrate, MigrateCommand

app.register_blueprint(admin)
app.register_blueprint(public)

migrate = Migrate(app, db)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'public.login'


@login_manager.user_loader
def load_user(user_id):
    user = User.query.get(int(user_id))
    return user
